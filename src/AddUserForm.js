import React, { useState } from 'react';
import Select from 'react-select';

const options1 = [
  { value: 'jogn', label: 'john' },
  { value: 'jack', label: 'jack' },
  { value: 'daniel', label: 'daniel' },
];

const options2 = [
  { value: 'pending', label: 'pending' },
  { value: 'available', label: 'available' },
  { value: 'free', label: 'free' },
];

const AddUserForm = (props) => {
  // const initialFormState = { id: null, adress: '', status: '', projectManager: '' };
  const [address, setAddress] = useState("");
  // const [projectManager, setProjectManager] = useState("");
  const [scopeWork, setScopeWork] = useState(null);
  const [projectManager, setProjectManager] = useState(null);
  
  return (
    <div className="container">
    <form
      onSubmit={(event) => {
        event.preventDefault();
        if (!address || !scopeWork) return;
        props.addUser({ address, scopeWork, projectManager });
        setAddress("");
        // setProjectManager("");
        setScopeWork(null);
        setProjectManager(null);
      }} 
    >
      <div className="form-group">
        <label>Adress</label>
        <input
          type="text"
          className="form-control my-1"
          placeholder="address"
          value={address}
          onChange={(e) => setAddress(e.target.value)}
        />
      </div>
      {/* <div className="form-group">
        <label>projectManager</label>  
          <input
          type="text"
          className="form-control my-1"
          placeholder="projectManager "
          value={projectManager}
          onChange={(e) => setProjectManager(e.target.value)}
        />
      </div> */}
      <div className="form-group">
        <label>ProjectManager</label>
        <Select
          options={options1}
          name="projectManager"

          isMulti
          classNamePrefix="select"
          className="basic-multi-select my-1"
          onChange={(e) => setProjectManager(e)}

        />
      </div>
      <div className="form-group">
        <label>Scope of Work</label>
        <Select
          options={options2}
          name="scopeWork"
          isMulti
          classNamePrefix="select"
          className="basic-multi-select my-1"
          onChange={(e) => setScopeWork(e)}

        />
      </div>
      <button className="btn btn-outline-success">Add new user</button>
    </form>
    </div>
  );
};

export default AddUserForm;