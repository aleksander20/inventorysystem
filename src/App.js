import React, { useState, useEffect } from "react";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import InventoryHeader from "../src/Components/InventoryHeader";
import UserTable from "./Components/UserTable";
import AddUserForm from "./AddUserForm";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import SidebarInventory from "./Components/SidebarInventory";
import Admin from "./Components/admin";

const App = () => {
  // Data
  let usersData = [
    { id: 1, adress: "one", status: "pendig", projectManager: "" },
    { id: 2, adress: "two", status: "false", projectManager: "" },
    { id: 3, adress: "three", status: "true", projectManager: "" },
  ];

  const initialFormState = { id: null, adress: "", stats: "", pm: "" };

  const [users, setUsers] = useState(usersData);
  // const [currentUser, setCurrentUser] = useState(initialFormState);
  // const [editing, setEditing] = useState(false);
  const addUser = user => {
    console.log(user);
    user.id = users.length + 1;
    user.status = "pending";
    const newUsers = usersData.push(user);
    setUsers(newUsers);

    if (JSON.parse(localStorage.getItem("UsersInventory")) === null) {
      const users = [];
      localStorage.setItem("UsersInventory", JSON.stringify(users));
    }
    const allUsers = JSON.parse(localStorage.getItem("UsersInventory"));
    allUsers.push(user);
    localStorage.setItem("UsersInventory", JSON.stringify(allUsers));
    console.log(allUsers);
  };
  // const local_storage_key ='UsersInventory';
  // useEffect(() => {
  //   const storedUsers = JSON.parse(localStorage.getItem(local_storage_key))
  //    setUsers(storedUsers)
  //  }, [])
  //  useEffect(() => {
  //    localStorage.setItem(local_storage_key,JSON.stringify(users))
  //  }, [users])

  return (
    <Router>
      <div>
        <>
          <InventoryHeader />

          {/* <AddUserForm/> */}
          {/* <SidebarInventory/> */}

          <Switch>
            <Route
              exact
              path="/"
              component={() => <UserTable users={users} />}
            />
            <Route
              path="/AddUserForm"
              component={() => <AddUserForm addUser={addUser} />}
            />
            <Route path="/admin" component={() => <Admin users={users} />} />
          </Switch>
        </>
      </div>
    </Router>
  );
};

export default App;