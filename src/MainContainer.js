import React from "react";

const mainContainer = props => {
  const { setSidebarState } = props;
  return (
    <div style={{ paddingTop: "20px", backgroundColor: "purple", flexGrow: 1 }}>
      <button onClick={() => setSidebarState(prevState => !prevState)}>
        toggleSidebar
      </button>
    </div>
  );
};

export default mainContainer;
