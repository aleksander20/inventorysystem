import React from 'react';
import { Link } from 'react-router-dom';
import '../Components/TableUserStyle.css'
const UserTable = (props) => {

  const usersArray = JSON.parse(localStorage.getItem("UsersInventory")).map((user, idx) => (
    <tr key={idx}>
      <td> <Link className="table-links" to="/admin">{user.address}</Link></td>
      <td><Link className="table-links" to="/admin">{user.scopeWork.map((opt) => opt.label + " ")}</Link></td>

    </tr>
      ))
   
  


  return (
    <div className="container">
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Adress</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          
        {usersArray}
         
        </tbody>
      </table>
      <Link to="/AddUserForm" className="btn btn-success">
        Add New
    </Link>
    </div>
  )
};

export default UserTable;