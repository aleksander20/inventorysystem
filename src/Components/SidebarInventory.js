import React, { useState } from "react";
import { Link } from "react-router-dom";
import "../Components/Sidebar.css";

export default function SidebarInventory(props) {
  const { sidebarState } = props;
  const [open, setOpen] = useState(false);

  return (
    <div
      className="sidebar"
      style={{
        backgroundColor: "#e8e8e8",
        marginLeft: sidebarState ? "0" : "-200px",
        minWidth: "200px",
      }}
    >
      <div style={{ width: "100%", textAlign: "center" }}>
        {/* <button onClick={() => setOpen((open) => !open)}>buttonopen</button> */}
      </div>

      <div className="sidebar-category">
        {/* {open && (
          <>
            <a href="#">Category</a>
            <a href="#">Category2</a>
            <a href="#">Category3</a>
            <a href="#">Category4</a>
          </>
        )} */}
        <>
          <Link to="/admin">Home</Link>
          <Link to="/admin/category1">Category</Link>
          <Link to="/admin/category2">Category2</Link>
          <Link to="/admin/category3">Category3</Link>
          <Link to="/admin/category4">Category4</Link>
        </>
      </div>
    </div>
  );
}