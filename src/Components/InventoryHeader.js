import React,{useState} from 'react';
import { Link } from 'react-router-dom';
import {Dropdown,DropdownButton} from 'react-bootstrap'
import avatar from '../Components/images/avatar.png';
import '../Components/HeaderStyles.css'
function InventoryHeader() {
  const[toogler,setToogler]=useState(false)
  return (
    <div>
      <nav className="navbar navbar-light bg-light navbar-expand-sm">
        <a className="navbar-brand" href="/">
          Inventory
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbar-list-4"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        
        {/* <div className="mx-2">
          <Link to="/admin">admin</Link>
        </div>
        <div className="mx-2">
          <Link to="/addUserForm">Add form</Link>
        </div> */}
        <div className="collapse navbar-collapse justify-content-end" id="navbar-list-4">
        
          <ul className="navbar-nav">
          <Dropdown>
          <Dropdown.Toggle  id="dropdown-basic" className="dropdown-component">
            <img
              src={avatar}
              width="40"
              height="40"
              className="rounded-circle"
            />
          </Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item href="#/action-1">Logout </Dropdown.Item>
            <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
            <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>

          </ul>
     
        </div>
      </nav>
    </div>
  );
}

export default InventoryHeader;
