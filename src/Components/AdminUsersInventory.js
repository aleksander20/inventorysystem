import React from "react";

const AdminUsersInventory = (props) => {
  const { user } = props;

  return (
    <>
      <tr>
        <td>{user.address}</td>
        {/* <td>{user.status}</td> */}
        <td>{user.scopeWork.map((opt) => opt.label + " ")}</td>
        <td>{user.projectManager.map((opt) => opt.label + " ")}</td>
      </tr>
    </>
  );
};

export default AdminUsersInventory;