import React from "react";
import SidebarInventory from "../Components/SidebarInventory.js";
import AdminUsersInventory from "../Components/AdminUsersInventory";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { BrowserRouter as Router, Route } from "react-router-dom";
class admin extends React.Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     users: [
  //       {
  //         // address: "",
  //         status: '',
  //         scopeWork: [{ label: '' }],
  //         projectManager: [{ label: '' }],
  //       },
  //     ],
  //   };
  // }

  state = {
    users: [
      {
        // address: "",
        status: "",
        scopeWork: [{ label: "" }],
        projectManager: [{ label: "" }],
      },
    ],
    sidebarState: 0,
  };

  toggleSidebar = () => {
    this.setState({ sidebarState: this.state.sidebarState ? 0 : 1 });
  };

  componentDidMount = () => {
    this.setState({
      users: JSON.parse(localStorage.getItem("UsersInventory")),
    });

    console.log(this.state.users);
  };

  render() {
    const inventory = this.props.inventory;

    let tableItems = null;

    if (this.state.users) {
      tableItems = this.state.users.map((user, index) => {
        console.log(user);
        return (
          <AdminUsersInventory inventory={inventory} user={user} key={index} />
        );
      });
    }

    return (
      <Router>
        <div style={{ display: "flex" }}>
          <SidebarInventory sidebarState={this.state.sidebarState} />
          <div className="content" style={{ flexGrow: 1 }}>
            <div>
              <button onClick={() => this.toggleSidebar()}>
                <FontAwesomeIcon icon={faBars} />
              </button>
            </div>
            <table className="table">
              <thead className="thead-dark">
                <tr>
                  <th scope="col">Address</th>
                  <th scope="col">Scope of work</th>
                  <th scope="col">Project Manager</th>
                </tr>
              </thead>
              <tbody>
                <Route exact path="/admin">
                  {tableItems}
                </Route>
                <Route path="/admin/category1">{tableItems}</Route>
                <Route path="/admin/category2">{tableItems}</Route>
                <Route path="/admin/category3">{tableItems}</Route>
                <Route path="/admin/category4">{tableItems}</Route>
              </tbody>
            </table>
          </div>
        </div>
      </Router>
    );
  }
}

export default admin;